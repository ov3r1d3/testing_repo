package main;

import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by rouse on 28/2/2018.
 * UpstreamTest
 */
public class Guessing_Game {

    private static int moves_counter = 0;
    private static int magic_number;
    private static int user_guess;
    private static int max_number;

    private static boolean game_complete;
    private static boolean do_over = true;

    private static long start_time;
    private static long elapsedTimeMillis;

    public static void main(String[] arg) throws InterruptedException {
        System.out.println("Welcome to Guessing Game!!!\n");
        while(do_over){

            System.out.println("Choose the difficulty of the game:");
            ChooseDifficulty();
            System.out.println("You have to guess a number between 0 & "+ max_number + "! We will give a clue every time you" +
                    " make a guess! Try your best to find in fewer attempts and in the fastest time!");

            Thread.sleep(1000);
            magic_number = gen_Random(max_number);
            System.out.println("Are you ready? ( Type yes to start )");
            ReadYes("");
            GameStart();
            start_time = System.currentTimeMillis();


            while(!game_complete){
                Thread.sleep(5);
                if (moves_counter == 0){
                    System.out.println("Start Guessing! ! ! !");
                }
                user_guess = ReadInt("",0,max_number);
                moves_counter++;
                if(user_guess > magic_number){
                    System.out.println(" ( ↓ ) Go down ");
                } else if ( user_guess < magic_number ){
                    System.out.println(" ( ↑ ) Go up ");
                } else {
                    game_complete = true;
                    elapsedTimeMillis = System.currentTimeMillis()-start_time;
                    System.out.println("----------------------------------");
                    System.out.println("Congratulations!!!! You found the magic Number. It was the marvelous " + magic_number);

                    System.out.println("----------------------------------");
                    System.out.println("----------------------------------");
                    System.out.println("Kindly find below your statistics:");
                    System.out.println("Number of guesses: " + moves_counter);
                    System.out.println("Time to Complete: " + elapsedTimeMillis/1000F + " seconds");
                    System.out.println("----------------------------------");
                    System.out.println("----------------------------------");
                }
            }
            System.out.println("If you want to try your skills again at another shot at the game type Go !! Anything else you type" +
                    " and we will be sad to see you go :( ");
            do_over = ReadChoice("");
            moves_counter=0;
            game_complete=false;
        }
        System.out.println("Thank you for playing the game ! We hope you had fun :) ");
        System.out.println("Please rate the game with 5 stars!");

    }


    private static int ReadInt(String welcome_message,int min,int max) throws InterruptedException {
        if(!Objects.equals(welcome_message, "")) System.out.println(welcome_message);

        boolean validInput = false;
        int val = 0;

        while(!validInput){
            Thread.sleep(500);
            Scanner scan = new Scanner(System.in);
            if(scan.hasNextInt()){
                val = scan.nextInt();
                if( val<(max+1) && val >=min){
                    validInput = true;
                } else {
                    System.out.println("Please enter a number between " + min +" & "+ max +"!");
                }
            }else{
                System.out.println("Oh rly?");
            }
        }
        return val;
    }

    private static String ReadYes(String welcome_message) throws InterruptedException {
        if(!Objects.equals(welcome_message, "")) System.out.println(welcome_message);

        boolean validInput = false;
        String phrase = null;

        while(!validInput){
            Thread.sleep(500);
            Scanner scan = new Scanner(System.in);
            phrase = scan.nextLine();
            if(phrase.equals("yes")){
                validInput = true;
            } else {
                System.out.println("Please enter yes if u want to start");
            }
        }
        return phrase;
    }

    private static boolean ReadChoice(String welcome_message) throws InterruptedException {
        System.out.println(welcome_message);
        boolean choice = false;
        String phrase;

        Scanner scan = new Scanner(System.in);
        phrase = scan.nextLine();
        if(phrase.matches("Go") || phrase.matches("go")) choice = true;

        return choice;
    }

    private static void GameStart() throws InterruptedException {
        System.out.println("Game starting in ...");
        for(int i=5;i>0;i--){
            System.out.println(i);
            Thread.sleep(1000);
        }
        System.out.println("------------------------------");
        System.out.println("And the game begins ! ! !  ! !");
    }


    private static int gen_Random(int max){
        Random rand = new Random();
        return rand.nextInt(max) + 1;
    }

    private static void ChooseDifficulty() throws InterruptedException {
        System.out.println("1.Very easy");
        System.out.println("2.Easy");
        System.out.println("3.Normal");
        System.out.println("4.Hard");
        System.out.println("5.Very hard");
        System.out.println("6.Extreme");
        System.out.println("7.Impossible");

        switch (ReadInt("",1,7)){
            case 1:
                max_number = 10;
                System.out.println("Pfff..");
                break;
            case 2:
                System.out.println("Hmmm..");
                max_number = 50;
                break;
            case 3:
                System.out.println("Okkkk..");
                max_number = 100;
                break;
            case 4:
                System.out.println("Way to go!");
                max_number = 1000;
                break;
            case 5:
                System.out.println("Wow...");
                max_number = 100000;
                break;
            case 6:
                System.out.println("You r the man!");
                max_number = 1000000;
                break;
            case 7:
                System.out.println("Are you serious !??!?! We are talking about 9 zeros !! ! ! !");
                max_number = 1000000000;
                break;
        }

    }




}
