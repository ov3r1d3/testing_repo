package main;

import java.util.Objects;
import java.util.Scanner;

/**
 * Created by rouse on 28/2/2018.
 * UpstreamTest
 */
public class SortingTest {

    private static int sorting_choice;


    //List<Integer> numbers_array = new ArrayList<Integer>();

    private static long start_time;

    public static void main(String[] arg) throws InterruptedException {
        System.out.println("Welcome to Sorting Test!");
        System.out.println("How many number are you sorting?");
        int count = ReadInt("",0,50);
        int[] numbers_array = new int[count];
        System.out.println("Please input "+ count + " numbers ranging from 0 until 1000");
        for (int i = 0 ; i < count; i++){
            numbers_array[i] = ReadInt("Please input Number "+ (i+1),0,1000);
        }

        System.out.println("Choose your sorting method:");
        System.out.println("1: Bubble Sort");
        System.out.println("2: Insertion Sort");
        System.out.println("3: Quick Sort");
        sorting_choice = ReadInt("",1,3);

        switch (sorting_choice){

            case 1:
                start_time = System.nanoTime();
                Bubble_Sort(numbers_array);
                System.out.println("Array sorted in " + (System.nanoTime()- start_time)+ " ns");
                break;
            case 2:
                start_time = System.nanoTime();
                Insertion_Sort(numbers_array);
                System.out.println("Array sorted in " + (System.nanoTime()- start_time)+ " ns");
                break;
            case 3:
                start_time = System.nanoTime();
                Quick_Sort(numbers_array,0,numbers_array.length-1);
                System.out.println("Array sorted in " + (System.nanoTime()- start_time)+ " ns");
                break;
        }
        System.out.println("Your sorted array is: ");
        printArray(numbers_array);
    }




    private static int ReadInt(String welcome_message,int min,int max) throws InterruptedException {
        if(!Objects.equals(welcome_message, "")) System.out.println(welcome_message);

        boolean validInput = false;
        int val = 0;

        while(!validInput){
            Thread.sleep(500);
            Scanner scan = new Scanner(System.in);
            if(scan.hasNextInt()){
                val = scan.nextInt();
                if( val<(max+1) && val >=min){
                    validInput = true;
                } else {
                    System.out.println("Please enter a number between " + min +" & "+ max +"!");
                }
            }else{
                System.out.println("Oh rly?");
            }
        }
        return val;
    }

    private static void Insertion_Sort(int array[]) {
        int n = array.length;
        for (int i=1; i<n; ++i)
        {
            int index = array[i];
            int j = i-1;
            while (j>=0 && array[j] > index)
            {
                array[j+1] = array[j];
                j = j-1;
            }
            array[j+1] = index;
        }
    }

    private static void Bubble_Sort(int array[]) {
        int n = array.length;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (array[j] > array[j+1]) {
                    int tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                }
    }

    private static void printArray(int array[]) {
        int n = array.length;
        for (int item : array) {
            System.out.print(item + " ");
        }

    }


    private static int partition(int array[], int start_index, int end_index) {
        int pivot = array[end_index];
        int i = (start_index-1);
        for (int j=start_index; j<end_index; j++) {
            if (array[j] <= pivot)
            {
                i++;
                int tmp = array[i];
                array[i] = array[j];
                array[j] = tmp;
            }
        }
        int tmp = array[i+1];
        array[i+1] = array[end_index];
        array[end_index] = tmp;
        return i+1;
    }


    private static void Quick_Sort(int array[], int start_index, int end_index) {
        if (start_index < end_index)
        {
            int partition = partition(array, start_index, end_index);

            Quick_Sort(array, start_index, partition-1);
            Quick_Sort(array, partition+1, end_index);
        }
    }

}
