package main;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by rouse on 27/2/2018.
 * UpstreamTest
 */
public class Ceasar_Cipher {

    private static int shiftIndex;
    private static String phrase_to_Cipher;

    private static String Ceasar_UpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static char[] Ceasar_Upper = Ceasar_UpperCase.toCharArray();
    private static String Ceasar_LowerCase = Ceasar_UpperCase.toLowerCase();
    private static char[] Ceasar_Lower = Ceasar_LowerCase.toCharArray();

    public static void main(String[] arg) throws InterruptedException {

        System.out.println("Welcome to Ceasar Cypher!!!\n");

        shiftIndex = ReadInt("Please input the shift index");
        phrase_to_Cipher = ReadString("Please input the phrase to cipher");

        System.out.println("Your encrypted string is:");
        System.out.println(EncryptString(phrase_to_Cipher, shiftIndex));
    }

    private static int ReadInt(String welcome_message) throws InterruptedException {
        System.out.println(welcome_message);

        boolean validInput = false;
        int val = 0;

        while(!validInput){
            Thread.sleep(500);
            Scanner scan = new Scanner(System.in);
            if(scan.hasNextInt()){
                val = scan.nextInt();
                if( val<26 && val >=0){
                    validInput = true;
                } else {
                    System.out.println("Please enter a number between 0 & 25!");
                }
            }else{
                System.out.println("Oh rly?");
            }
        }

        System.out.println("Your cipher will be rotated by : " + val);
        return val;
    }


    private static String ReadString(String welcome_message) throws InterruptedException {
        System.out.println(welcome_message);
        boolean validInput = false;
        String phrase = null;

        while(!validInput){
            Thread.sleep(500);
            Scanner scan = new Scanner(System.in);
            phrase = scan.nextLine();
            if(phrase.matches("[a-zA-Z ]*$")){
                validInput = true;
            } else {
                System.out.println("Please enter a string with characters from the alphabet only");
            }
        }
        System.out.println("You entered: " + phrase);
        return phrase;
    }

    private static String EncryptString(String str, int keyLength) {
        String encrypted_String="";
        char[] character_array = str.toCharArray();
        int char_index;

        for(int i=0;i<str.length();i++)
        {
            char character = character_array[i];
            if(Character.isUpperCase(character))
            {
                char_index=(findIndex(character)+keyLength)%Ceasar_UpperCase.length();
                encrypted_String=encrypted_String+ Ceasar_Upper[char_index];
            }
            else if(Character.isLowerCase(character))
            {
                char_index=(findIndex(character)+keyLength)%Ceasar_UpperCase.length();
                encrypted_String=encrypted_String+ Ceasar_Lower[char_index];
            } else if(Character.isSpaceChar(character)){
                encrypted_String=encrypted_String+ character;
            }
        }
        return encrypted_String;
    }

    private static int findIndex(Character letter){

        if(Character.isUpperCase(letter)){
            for(int i=0;i<Ceasar_Upper.length;i++) {
               if(letter.equals(Ceasar_Upper[i])){
                   return i;
               }
            }
        } else {
            for(int i=0;i<Ceasar_Lower.length;i++) {
                if(letter.equals(Ceasar_Lower[i])){
                    return i;
                }
            }
        }
        return 0;
    }

}
